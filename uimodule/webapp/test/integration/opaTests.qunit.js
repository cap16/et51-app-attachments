/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require(["de/bit/et51/ui5/attachments/test/integration/AllJourneys"], function () {
        QUnit.start();
    });
});
