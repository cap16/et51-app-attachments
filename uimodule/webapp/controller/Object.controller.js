sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"../model/formatter",
	"sap/m/upload/Uploader",
	"sap/ui/Device"
], function (BaseController, JSONModel, History, formatter, Uploader, Device) {
	"use strict";

	var CustomUploader = Uploader.extend("de.bit.et50.user.attachment.attachment.controller.CustomUploader", {
		metadata: {}
	});

	CustomUploader.prototype._us = function(us) {
		this.us = us;
	};

	CustomUploader.prototype.uploadItem = function (oItem, aHeaderFields) {
		console.log("--- custom uploader");
		//console.log(uploadUrl);
		//console.log(aHeaderFields);

		var oXhr = new window.XMLHttpRequest(),
			oFile = oItem.getFileObject(),
			that = this,
			oRequestHandler = {
				xhr: oXhr,
				item: oItem
			};

		var uploadUrl = "";
		if (aHeaderFields) {
			aHeaderFields.forEach(function (oHeader) {
				if (oHeader.getKey() === "target") {
					uploadUrl = oHeader.getText();
				}
			});
		}
	
		oXhr.open("PUT", uploadUrl, true);

		if ((Device.browser.edge || Device.browser.internet_explorer) && oFile.type && oXhr.readyState === 1) {
			oXhr.setRequestHeader("Content-Type", oFile.type);
		}

		if (aHeaderFields) {
			aHeaderFields.forEach(function (oHeader) {
				oXhr.setRequestHeader(oHeader.getKey(), oHeader.getText());		
			});
		}

		oXhr.upload.addEventListener("progress", function (oEvent) {
			that.fireUploadProgressed({
				item: oItem,
				loaded: oEvent.loaded,
				total: oEvent.total,
				aborted: false
			});
		});

		oXhr.onreadystatechange = function () {
			var oHandler = that._mRequestHandlers[oItem.getId()];
			if (this.readyState === window.XMLHttpRequest.DONE && !oHandler.aborted) {
				//that.fireUploadCompleted({item: oItem});
				that.us.fireUploadCompleted({item: oItem});
			}
		};

		this._mRequestHandlers[oItem.getId()] = oRequestHandler;
		oXhr.send(oItem.getFileObject());
		this.fireUploadStarted({item: oItem});
	};

	return BaseController.extend("de.bit.et50.user.attachment.attachment.controller.Object", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		/**
		 * Called when the worklist controller is instantiated.
		 * @public
		 */
		onInit : function () {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page shows busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
					busy : true,
					delay : 0
				});
			this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);
			this.setModel(oViewModel, "objectView");

			var oCustomUploader = new CustomUploader();
			oCustomUploader._us(this._getUploadSet());
			this._getUploadSet().setUploader(oCustomUploader);
			this._getUploadSet().getList().setMode(sap.m.ListMode.SingleSelectLeft);						
		},
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */


		/**
		 * Event handler  for navigating back.
		 * It there is a history entry we go one step back in the browser history
		 * If not, it will replace the current entry of the browser history with the worklist route.
		 * @public
		 */
		onNavBack : function() {
			var sPreviousHash = History.getInstance().getPreviousHash();
			if (sPreviousHash !== undefined) {
				// eslint-disable-next-line sap-no-history-manipulation
				history.go(-1);
			} else {
				this.getRouter().navTo("worklist", {}, true);
			}
		},

		onApprove: function(oEvent) {
			console.log("onApprove");

			var oContext = this.getView().getBindingContext();
			var sPath = oContext.getPath();
			this.getModel().setProperty(sPath+"/status", 2)
			console.log(sPath);
			
			this.getModel().submitChanges({
				success: function(oResponse){
                    var oMessage = new sap.ui.core.message.Message({
                        message: "Anfrage erfolgreich gesendet",
                        persistent: true,
                        type: sap.ui.core.MessageType.Success
                    });
                    var oMessageManager = sap.ui.getCore().getMessageManager();
                    oMessageManager.addMessages(oMessage);
                    }
				});

			this._getAccessToken();
		},

		/**
		 * get a new valid access token
		 * use refresh token to renew access token
		 */
		 _getAccessToken: function() {
			console.log("_getAccessToken");

			// parameters
			var baseUrl = "/oauth/token";
			var refresh_token = "4db32701a92043a7b2cc8a84282e897e-r";
			var grant_type = "refresh_token";
			var username = "sb-clone-38f5b8a9-4892-4726-8db8-2283f9102565!b65780|workflow!b10150";
			var client_credentials = "b77c97ff-40a7-438e-9849-66e19522b636$aTP8r42PRLKg0h6q17GQBGfrLe5VzXCwOVPyaRckuTM=";

			// prepare data
			var credentials = username + ":" + client_credentials;
			var base64Credentials = btoa(credentials);
			var url = baseUrl + "?refresh_token=" + refresh_token + "&grant_type="+grant_type;
			
			$.ajax({
	            url: url,
				beforeSend: function (xhr) {					
					xhr.setRequestHeader("Authorization", "Basic " + base64Credentials);
				},
	            context: this,
	            type: "GET",
	            success: this._getWorkflowStep
	        });
		},

		_getWorkflowStep: function(oJwt) {
			console.log("_getWorkflowStep " + oJwt);

			var auth = "Bearer " + oJwt.access_token;
			var url = "/workflow-service/rest/v1/workflow-instances";
			console.log(url);
			$.ajax({
	            url: url,
				beforeSend: function (xhr) {					
					xhr.setRequestHeader("Authorization", auth);
				},
	            context: this,
	            type: "GET",
				success: function(response) {
					console.log(response[0]);
					var sWorkflowInstanceId = response[0].id;
					this._sendIntermediateMessage(auth, sWorkflowInstanceId);
				},
				error: function(err) {
					console.log("error");
					console.log(err);
				}
	        });
		},


		/**
		 * Confirm that step was successfulle executed
		 */
		_sendIntermediateMessage: function(sAuth, sWorkflowInstanceId) {
			//console.log("_sendIntermediateMessage: " + sAuth + " " + sWorkflowInstanceId);

			var oBody = {};
			oBody.definitionId = "attachmentadded";
			oBody.workflowInstanceId = sWorkflowInstanceId;

			var url = "/workflow-service/rest/v1/messages";
			var contentType = "application/json";
			
			console.log(url);
			$.ajax({
	            url: url,
				beforeSend: function (xhr) {					
					xhr.setRequestHeader("Authorization", sAuth);
					xhr.setRequestHeader("Content-Type", contentType);
				},
				context: this,
				dataType: "json",
	            data: JSON.stringify(oBody),
	            type: "POST",
	            success: function(response) {
					console.log(response);
					console.log(response[0].id);
					
				},
				error: function(error) {
					console.log("_sendIntermediateMessage error");
					console.log(error);
				}
	        });
		},

		onUploadSelectedButton: function(oEvent) {
			console.log("onUploadSelectedButton");

			var oUploadSet = this._getUploadSet();
			var iFilesToUpload = oUploadSet.getIncompleteItems().length;
			console.log("Files to upload: " + iFilesToUpload);

			var sUrl = this.getView().getBindingContext().getPath();
			console.log("upload url: " + sUrl);

						
			// take each file to upload and upload it
			oUploadSet.getIncompleteItems().forEach(function (oItem) {
				var sFilename = oItem.getProperty("fileName");
				var oHeaderItemFilename = new sap.ui.core.Item({key: "slug", text: sFilename});
				oUploadSet.addHeaderField(oHeaderItemFilename);
				// file meta data
				var oData = {};
				oData.mediaType = "application/msword";
				oData.name = sFilename;
				var oItemUpload = oItem;
				// create a new file entry
				this.getModel().create(sUrl + "/attachment", oData, {
					success: function(resp) {
						console.log("success");
						console.log("ID: " + resp.ID);
						console.log("Claim ID: " + resp.claim_ID);
						
						// upload file content to given ID	
						var oUploadSet = this._getUploadSet();
						var sUploadUrl = "/Attachments("+resp.ID+")/file";
						oUploadSet.setUploadUrl(sUploadUrl);

						// set url to header
						var oHeaderItemUrl = new sap.ui.core.Item({key: "target", text: "/v2/bit"+sUploadUrl});
						oUploadSet.addHeaderField(oHeaderItemUrl);
						oUploadSet.uploadItem(oItemUpload);
						/*
						oUploadSet.getIncompleteItems().forEach(function (oItem) {
							oUploadSet.uploadItem(oItem);
							oUploadSet.removeHeaderField(oHeaderItemFilename);
						});					
						*/
					}.bind(this),
					error: function(err) {
						console.log("error");
						console.log(err);
					}
				});

			}.bind(this));					
		},

		onUploadCompleted: function(oEvent) {
			console.log("onUploadCompleted");
			var oItem = oEvent.getParameter("item");

			var oUploadSet = this._getUploadSet();
			oUploadSet.removeIncompleteItem(oItem);
		},

		_getUploadSet: function() {
			var sFragmentId = this.getView().createId("attachments");
			var oUploadSet = sap.ui.core.Fragment.byId(sFragmentId, "UploadSet");
			return oUploadSet;
		},

		onBeforeUploadStarts: function(oEvent) {
			console.log("onBeforeUploadStarts");
			var oUploadSet = oEvent.getSource();
			
			var sCsrf = this.getModel().getSecurityToken();
			console.log(sCsrf);
			var oHeaderItemCsrf = new sap.ui.core.Item({key: "x-csrf-token", text: sCsrf});
			oUploadSet.addHeaderField(oHeaderItemCsrf);
		},
		
		/* =========================================================== */
		/* internal methods                                            */
		/* =========================================================== */

		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched : function (oEvent) {
			var sObjectId =  oEvent.getParameter("arguments").objectId;
			this._bindView("/Claim" + sObjectId);
		},

		/**
		 * Binds the view to the object path.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound
		 * @private
		 */
		_bindView : function (sObjectPath) {
			var oViewModel = this.getModel("objectView");

			this.getView().bindElement({
				path: sObjectPath,
				parameters: {
					expand: 'user,manager,attachment'
				},
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function () {
						oViewModel.setProperty("/busy", true);
					},
					dataReceived: function () {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},

		_onBindingChange : function () {
			var oView = this.getView(),
				oViewModel = this.getModel("objectView"),
				oElementBinding = oView.getElementBinding();
			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("objectNotFound");
				return;
			}

			
			/*
			var oResourceBundle = this.getResourceBundle();
			oView.getBindingContext().requestObject().then((function (oObject) {
				var sObjectId = oObject.ID,
					sObjectName = oObject.ID;


				oViewModel.setProperty("/busy", false);
			
			}).bind(this));
			*/
		}

	});

});